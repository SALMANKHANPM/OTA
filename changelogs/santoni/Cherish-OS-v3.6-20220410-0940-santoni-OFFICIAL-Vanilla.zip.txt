Changelog v3.6 07/04/2022

* Updated April Security Patch
* Added Hide clock
* Added Mobile Data panel 
* Added Bluetooth Panel instead of full Settings
* Added WifiPanel on LongClick
* Added back QQS brightness slider 
* Added Black theme
* Added clear all button styles
• Redesigned User Interface
• Custom Dashboard Styles (AOSP,OOS11,OOS12)
• Improved Vivid Colors monet implementation
• Changed default Monet shades for better UI colors
• Added support vanilla builds 
* Fixed Quick tap FC
* Fixed qs clock white color
* Fixed volume panel position FC
* Updated SIMs QS icons 
* Updated translation from crowdin
* Improve other systems
